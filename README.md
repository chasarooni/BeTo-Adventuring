# BeTo - Adventuring

## For Game Masters

### Chests

The Adventuring Module supports changing chest tokens, it is probably not the best way to do it, but this is one of my first Modules. Every chest should consist of three tokens:

* Empty Chest Token
* Full Chest Token
* Closed Chest Token

The tokens also need to follow a strict naming scheme: `Chest:[id]:[type]`

Where `[id]` is the unique identifier of the chest (all 3 chest tokens should have the same `[id]`) and where `[type]` is `Empty`, `Open`, `Unlocked` or `Locked:DC:[DC]` with the `[DC]` is an integer.

Examples: `Chest:01:Empty` `Chest:01:Open` `Chest:01:Unlocked` `Chest:02:Locked:DC:12`

Players can interact with the chest by double-clicking the token.

### Mimics

The Mimics work in mostly the same way as the Chests with multiple tokens linked through a naming scheme: `Mimic:[id]` and `Mimic:[id]:Trigger`.

Currently, the Mimic does not start combat or attack yet but will only show the token and pause the game.

## For Developers

This module is still in early stages of development and if you have any hooks you want to hook into or if you have bugs / features, feel free to create a ticket.

### Hooks

- [none at the moment]

### Accessible Methods

- adventuring.functions
    - `switchTokens(activeToken, newToken): void`
    - `onPlaceableInteraction(placeable, callback): void`
    - `playerIsGM(): bool`
    - `findTokenByName(canvas, name): Token`
    - `socket` the `socketlib` socket
- adventuring.chests
    - `isChest(placeable): bool`
    - `getName(chest): string`
    - `getType(chest): string`
    - `isEmpty(chest): bool`
    - `getOpenToken(canvas, chest): Token`
    - `getEmptyToken(canvas, chest): Token`
- adventuring.mimics
    - `isMimicTrigger(placeable): bool`
    - `getMimicToken(canvas, mimic): Token`

### Future plans

- A discord server for bugs, feature requests and questions
- Proper localization instead of hard-coded English
- Compiling the JS files

Eventually I would like to create a custom actor type for chests and mimics in order to have a single object with the different token images in order to make it way more manageable.
