'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', beto => {
        beto.getModule('adventuring.functions').then(adventuring => {
            beto.registerModule('adventuring.mimics', factory(beto, adventuring));
        });
    });
})((beto, adventuring) => {
    adventuring.socket.register('notifyMimicInteraction', mimicTriggerId => {
        beto.getCanvas().then(canvas => {
            const mimicTrigger = beto.findTokenById(canvas, mimicTriggerId);
            adventuring.switchTokens(mimicTrigger, mimics.getMimicToken(canvas, mimicTrigger));
            game.togglePause(true, true);
        });
    });

    if (!adventuring.playerIsGM()) {
        const mimicInteraction = (mimic, actor) => {
            adventuring.socket.executeAsGM('notifyMimicInteraction', mimic.id, actor.id);
        };

        beto.onCanvasReady(canvas => {
            canvas.tokens.placeables.forEach(placeable => {
                if (!mimics.isMimicTrigger(placeable)) {
                    return;
                }
                adventuring.onPlaceableInteraction(placeable, mimicInteraction);
            });
        });
    }

    const mimics = {
        isMimicTrigger: placeable => placeable.name.startsWith('Mimic:') && placeable.name.endsWith(':Trigger'),
        getMimicToken: (canvas, mimic) => adventuring.findTokenByName(canvas, 'Mimic:' + mimic.name.split(':')[1]),
    };
    return mimics;
});
