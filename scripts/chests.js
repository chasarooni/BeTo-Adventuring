'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', beto => {
        beto.getModule('adventuring.functions').then(adventuring => {
            beto.registerModule('adventuring.chests', factory(beto, adventuring));
        });
    });
})((beto, adventuring) => {
    adventuring.socket.register('notifyChestInteraction', senderName => {
        ui.notifications.info(senderName + ' is interacting with a chest');
    });

    adventuring.socket.register('notifyUnlockAttempt', (chestId, senderName, successfulUnlock) => {
        beto.getCanvas().then(canvas => {
            const lockedChest = beto.findTokenById(canvas, chestId);
            if (successfulUnlock) {
                adventuring.switchTokens(lockedChest, chests.getOpenToken(canvas, lockedChest));
            }
        });
    });

    const doUnlockRoll = (lockedChest, actor) => {
        game.user.character.rollSkill('slt');
        const rollListener = (chatMessage, html, data) => {
            if (data.author.id !== game.user.id) {
                Hooks.once('renderChatMessage', rollListener);
                return;
            }
            const unlockDc = lockedChest.name.split(':')[4];
            const successfulUnlock = JSON.parse(data.message.roll).total >= unlockDc;
            adventuring.socket.executeAsGM('notifyUnlockAttempt', lockedChest.id, actor.name, successfulUnlock);
        };
        Hooks.once('renderChatMessage', rollListener);
    };

    if (adventuring.playerIsGM()) {
        Hooks.on('updateActor', chestActor => {
            beto.getCanvas().then(canvas => {
                if (chestActor.token === null || !chests.isChest(chestActor.token) || !chests.isEmpty(chestActor.token)) {
                    return;
                }
                adventuring.switchTokens(chestActor.token, chests.getEmptyToken(canvas, chestActor.token));
            });
        });
    } else {
        const openUnlockedChest = (chest, actor) => {
            adventuring.socket.executeAsGM('notifyUnlockAttempt', chest.id, actor.name, true);
        };

        const showLockedChestDialog = (chest, actor) => {
            adventuring.socket.executeAsGM('notifyChestInteraction', actor.name);
            // noinspection JSUnusedGlobalSymbols
            const dialog = new Dialog({
                title: 'Locked Chest',
                content: '<p>This chest is locked. Make a lock-picking check to open it.</p>',
                buttons: {
                    one: {
                        label: 'Roll',
                        icon: '<i class="fas fa-check"></i>',
                        callback: () => doUnlockRoll(chest, actor),
                    },
                },
            });
            dialog.render(true);
        };

        beto.onCanvasReady(canvas => {
            canvas.tokens.placeables.forEach(placeable => {
                if (!chests.isChest(placeable)) {
                    return;
                }
                switch (chests.getType(placeable)) {
                    case 'Empty':
                    case 'Open':
                        break;
                    case 'Unlocked':
                        adventuring.onPlaceableInteraction(placeable, openUnlockedChest);
                        break;
                    case 'Locked':
                        adventuring.onPlaceableInteraction(placeable, showLockedChestDialog);
                        break;
                    default:
                        throw new Error('Unknown type');
                }
            });
        });
    }

    const chests = {
        isChest: placeable => placeable.name.startsWith('Chest:'),
        getName: chest => chest.name.split(':')[1],
        getType: chest => chest.name.split(':')[2],
        isEmpty: chest => {
            return (
                chest.actor.items.size === 0
                && parseInt(chest.actor.data.data.currency.cp.value) === 0
                && parseInt(chest.actor.data.data.currency.ep.value) === 0
                && parseInt(chest.actor.data.data.currency.gp.value) === 0
                && parseInt(chest.actor.data.data.currency.pp.value) === 0
                && parseInt(chest.actor.data.data.currency.sp.value) === 0
            );
        },
        getOpenToken: (canvas, chest) => adventuring.findTokenByName(canvas, 'Chest:' + chests.getName(chest) + ':' + 'Open'),
        getEmptyToken: (canvas, chest) => adventuring.findTokenByName(canvas, 'Chest:' + chests.getName(chest) + ':' + 'Empty'),
    };
    return chests;
});
