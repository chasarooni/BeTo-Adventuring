'use strict';
(factory => {
    Hooks.once('beto.library.loaded', beto => {
        Hooks.once('socketlib.ready', () => {
            Hooks.once('ready', () => {
                beto.registerModule('adventuring.functions', factory(beto, window.socketlib));
            });
        });
    });
})((beto, socketlib) => {
    const placeableClickCallbacks = {};

    const switchTokens = (activeToken, newToken) => {
        newToken.update({ hidden: false });
        activeToken.update({ hidden: true });
        setTimeout(activeToken.delete);
    };

    const onDoubleClick = (canvas, placeable) => {
        const tooFar = beto.getActivePlayerTokens(canvas).every(actorToken => {
            if (beto.canInteract(actorToken, placeable)) {
                placeableClickCallbacks[placeable.id].forEach(callback => callback(placeable, actorToken));
                return false; // Stop Loop
            }
            return true; // Continue Loop
        });
        if (tooFar) {
            ui.notifications.warn('Move closer to interact with the object');
        }
    };

    let lastClickTime = 0;
    const registerMouseDown = (placeable) => {
        placeable.on('mousedown', event => {
            beto.getCanvas().then(canvas => {
                if (event.data.originalEvent.button !== 0) return; // Only support standard left-click
                const now = Date.now();
                if ((now - lastClickTime) <= 250) {
                    onDoubleClick(canvas, placeable);
                    lastClickTime = 0;
                    return;
                }
                lastClickTime = now;
            });
        });
    };

    const onPlaceableInteraction = (placeable, callback) => {
        if (!placeableClickCallbacks.hasOwnProperty(placeable.id)) {
            placeableClickCallbacks[placeable.id] = [];
            registerMouseDown(placeable);
        }
        placeableClickCallbacks[placeable.id].push(callback);
    };

    return {
        switchTokens,
        onPlaceableInteraction,
        playerIsGM: () => game.users.current.data.role === foundry.CONST.USER_ROLES.GAMEMASTER,
        findTokenByName: (canvas, name) => canvas.tokens.placeables.find(placeable => placeable.name === name),
        socket: socketlib.registerModule('beto-adventuring'),
    };
});
